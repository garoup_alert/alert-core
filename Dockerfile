FROM maven:3-jdk-8-alpine

ENV TZ=Asia/Seoul

WORKDIR /usr/src/app

COPY ./alert-core-boot/target/alert-core-boot-1.0-spring-boot.jar /usr/src/app

EXPOSE 8086

CMD java -jar alert-core-boot-1.0-spring-boot.jar
