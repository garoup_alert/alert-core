package kr.hbh.alert.domain.entity;

import lombok.Data;

@Data
public class Payload {

	public Payload(long chatId, String text){
		this.chatId = chatId;
		this.text = text;
	}

	private long chatId;

	private String text;
}
