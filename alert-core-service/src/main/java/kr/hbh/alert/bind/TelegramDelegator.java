package kr.hbh.alert.bind;

import com.pengrad.telegrambot.Callback;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.request.BaseRequest;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.BaseResponse;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class TelegramDelegator {

    private String botToken;

    public TelegramDelegator(String botToken){
        this.botToken = botToken;
    }

    public void sendMessage(long chatId, String message){
        TelegramBot bot = new TelegramBot(botToken);

        //비동기 전송
        bot.execute(new SendMessage(chatId, message), new Callback() {
            @Override
            public void onResponse(BaseRequest request, BaseResponse response) {
                log.info("chatId[" + chatId + "] send status = " + response.isOk());
            }
            @Override
            public void onFailure(BaseRequest request, IOException e) {
                log.error("chatId[" + chatId + "] send fail = " + e.getMessage());
            }
        });
    }

}
