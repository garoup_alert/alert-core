package kr.hbh.alert.logic;

import com.google.gson.GsonBuilder;
import kr.hbh.alert.bind.TelegramDelegator;
import kr.hbh.alert.domain.entity.Payload;
import kr.hbh.alert.domain.service.AlertCoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AlertCoreLogic implements AlertCoreService {

    @Value("${telegram.bot.token}")
    private String telegramBotToken;

    @Override
    public void alertSend(String payload) {
        Payload request = new GsonBuilder().create().fromJson(payload, Payload.class);
        TelegramDelegator delegator = new TelegramDelegator(telegramBotToken);
        delegator.sendMessage(request.getChatId(), request.getText());
    }
}
