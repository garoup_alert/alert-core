package kr.hbh.alert.listen;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface AlertSink {

    String ALERT_INPUT = "alertInput";

    @Input(AlertSink.ALERT_INPUT)
    SubscribableChannel bypassChannel();

}
