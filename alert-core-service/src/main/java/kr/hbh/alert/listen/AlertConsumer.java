package kr.hbh.alert.listen;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;

import kr.hbh.alert.domain.service.AlertCoreService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableBinding(AlertSink.class)
public class AlertConsumer {

    private final AlertCoreService alertCoreService;

    public AlertConsumer(AlertCoreService alertCoreService){
        this.alertCoreService = alertCoreService;
    }

    @StreamListener(AlertSink.ALERT_INPUT)
    public void subscribe(@Payload String payload) {
        log.info("ALERT_CHANNEL subscribe!");
        log.debug("payload=" + payload);

        alertCoreService.alertSend(payload);
    }
}
